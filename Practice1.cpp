#include <vector>
#include <string>
#include <iostream>

using namespace std;

int main()
{
 vector <int> v = { 10, 14, 7, 69};
 
 for (int i= 0; i < v.size(); i++)
 {
	 cout << v[i] << " " ;
	 cout << endl;
	 }
	 
	v.push_back(100);
	
	for (int i= 0; i < v.size(); i++)
 {
	 cout << v[i] << " ";
	 cout << endl;
	 } 

	v.pop_back();
	
	for (int i= 0; i < v.size(); i++)
 {
	 cout << v[i] << " ";
	 cout << endl;
	 } 
	 
	 return 0;
	}
